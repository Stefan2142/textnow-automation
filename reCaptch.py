import time
import pyautogui
import speech_recognition as sr
import os
import subprocess
from queryAPI import bing, google, ibm

''' You'll need to update based on the coordinates of your setup '''
FIREFOX_ICON_COORDS = (25, 	 121) # Location of the Firefox icon on the side toolbar (to left click)
PRIVATE_COORDS		= (110,  106) # Location of "Open a new Private Window"
PRIVATE_BROWSER 	= (460, 500) # A place where the background of the Private Window will be

PERSON_AVATAR		= (1873, 79) # A place of avatar to add new person of chrome
AVATAR_COLOR		= '#DEDEDE' 
GEST_USER			= (1632, 561)
SCROLL_DOWN			= (1871, 1075)
MANAGE_PEOPLE		= (1813, 1019)
ADD_PERSON_BUTTON 	= (1292, 851)
AP_BUTTON_COLOR 	= '#F7FAFF'
ADD_AVATAR 			= (1319, 845)
ADD_AVATAR_COLOR	= '#3080EA'
GOOGLE_ICON 		= (1021, 519)
GOOGLE_ICON_Y_COLOR	= '#FFCD40'
REMOVE_USER_MENU 	= (764, 338)
REMOVE_USER_BUTTON	= (746, 404)
REMOVE_CONFIRM		= (712, 584)
ClOSE_ADD_WINDOWS	= (609, 268)

PRIVATE_COLOR		= '#25003E'  # The color of the background of the Private Window
# PRIVATE_COLOR		= '#323639'  # The color of the background of the Private Window
SEARCH_COORDS 		= (755, 78) # Location of the Firefox Search box
REFRESH_COORDS      = (150, 76) # Refresh button
GOOGLE_LOCATION     = (485, 160) # Location of the TEXTNOW Icon after navigating to www.textnow.com/signup?ref=NonSale-GatewaySlot3
TEXTNOW_ICON1		= (150, 200) # Location of the TEXTNOW ICON
TEXTNOW_ICON_COLOR1	= '#B295EE'
TEXTNOW_ICON2		= (476, 203) # Location of the TEXTNOW ICON
TEXTNOW_ICON_COLOR2	= '#652CDE'

#GOOGLE_COLOR 		= '#642BDD'  # Color of the TEXTNOW Icon
GOOGLE_COLOR 		= '#652CDE'  # Color of the TEXTNOW Icon
CAPTCHA_COORDS		= (867, 420) # Coordinates of the empty CAPTCHA checkbox
TEMP_CHECK_COORDS	= (337, 45) # Temp location
CHECK_SIGNUP		= (932, 457)
CHECK_COORDS 		= (857, 420) # Location where the green checkmark will be
CHECK_COLOR 		= '#009E55'  # Color of the green checkmark
AUDIO_COORDS		= (966, 691) # Location of the Audio button
DOWNLOAD_COORDS		= (1030, 474) # Location of the Download button
AUDIO_PLAY_BAR		= (925, 572) # Location of Audio Player
PLAY_BAR_COLOR		= '#F1F3F4'
DOWNLOAD_AUDIO_ITME = (963, 678) # Location of Download MENU Item
SAVE_BUTTON 		= (1406, 805) # Save the auido file.
CLOSE_TAB			= (536, 43) # Close the Audio play tab
FINAL_COORDS  		= (974, 416) # Text entry box
VERIFY_COORDS 		= (1118, 541) # Verify button
EMAIL_INPUT			= (862, 330) # Location of email input
PASSWORD_INPUT		= (862, 390) # Location of password input
SIGNUP_BUTTON		= (910, 450)
NOCAPTCHA_SIGNUP_BUTTON = (915,450)
CONFIRM_CREATE 		= (640, 500)
CONFIRM_CREATE_COLOR= '#4C4C4C'
AREA_CODE_INPUT 	= (986, 429)
AREA_CODE_BUTTON	= (951, 507)

CAPTCHA_CHECK_BOX	= (866, 360)
AFTER_AUDIO_COORDS 	= (974, 702)
AFTER_DOWNLOAD_COORDS=(1035, 404)	
AFTER_AUDIO_PLAY_BAR= (892, 620)
AFTER_DOWNLOAD_AUDIO_ITME = (924, 724)
AFTER_SAVE_BUTTON 	= (1340, 797) # Save the auido file.
DL_CLOSE_BUTTON		= (1904, 1058)
AFTER_FINAL_COORDS	= (942, 360)
AFTER_VERIFY_COORDS = (1105, 468)

CONFIRM_NUMBER_BUTTON= (947, 522)
CONFIRM_NUMBER_COLOR= '#652CDE'



CLOSE_LOCATION		= (14, 14)


DOWNLOAD_LOCATION = "../"
''' END SETUP '''

noCaptcha = False
r = sr.Recognizer()

def runCommand(command):
	''' Run a command and get back its output '''
	proc = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
	return proc.communicate()[0].split()[0]

def waitFor(coords, color):
	''' Wait for a coordinate to become a certain color '''
	pyautogui.moveTo(coords)
	numWaitedFor = 0
	print (runCommand("eval $(xdotool getmouselocation --shell); xwd -root -silent | convert xwd:- -depth 8 -crop \"1x1+$X+$Y\" txt:- | grep -om1 '#\w\+'").decode("utf-8"))
	
	while color != runCommand("eval $(xdotool getmouselocation --shell); xwd -root -silent | convert xwd:- -depth 8 -crop \"1x1+$X+$Y\" txt:- | grep -om1 '#\w\+'").decode("utf-8"):
		time.sleep(.1)
		numWaitedFor += 1
		if numWaitedFor > 25:
			return -1
	return 0

def closeWinodws():

	pyautogui.moveTo(PERSON_AVATAR)
	pyautogui.click()
	time.sleep(2)

	pyautogui.moveTo(SCROLL_DOWN)   # Scroll down to click the manage the people.
	pyautogui.click()
	time.sleep(2)
	pyautogui.moveTo(MANAGE_PEOPLE)
	pyautogui.click()
	time.sleep(3)

	pyautogui.moveTo(REMOVE_USER_MENU)
	pyautogui.click()
	time.sleep(2)

	pyautogui.moveTo(REMOVE_USER_BUTTON)
	pyautogui.click()
	time.sleep(2)

	pyautogui.moveTo(REMOVE_CONFIRM)
	pyautogui.click()
	time.sleep(2)

	pyautogui.moveTo(ClOSE_ADD_WINDOWS)
	pyautogui.click()
	time.sleep(4)

	pyautogui.moveTo(CLOSE_LOCATION)
	pyautogui.click()
	time.sleep(2)
	pyautogui.click()
	time.sleep(2)

	# pyautogui.moveTo(ClOSE_ADD_WINDOWS)
	# pyautogui.click()
	# time.sleep(4)

	return 3





def downloadCaptcha(tempEmail):
	''' Navigate to demo site, input user info, and download a captcha. '''
	print("Opening Firefox")
	pyautogui.moveTo(FIREFOX_ICON_COORDS)
	pyautogui.rightClick()
	time.sleep(.5)
	pyautogui.moveTo(PRIVATE_COORDS)
	pyautogui.click()
	time.sleep(5)

	pyautogui.moveTo(PERSON_AVATAR)
	if waitFor(PERSON_AVATAR, AVATAR_COLOR) == -1: # Wait for browser to load
		return -1
	pyautogui.click()
	time.sleep(1)

	pyautogui.moveTo(SCROLL_DOWN)   # Scroll down to click the manage the people.
	pyautogui.click()
	time.sleep(1)
	pyautogui.moveTo(MANAGE_PEOPLE)
	pyautogui.click()
	time.sleep(3)

	pyautogui.moveTo(ADD_PERSON_BUTTON)
	if waitFor(ADD_PERSON_BUTTON, AP_BUTTON_COLOR) == -1: # Wait for dialog to add preson.
		return -1
	pyautogui.click()
	time.sleep(2)

	pyautogui.typewrite(tempEmail, interval=0.1)
	time.sleep(1)
	pyautogui.moveTo(ADD_AVATAR)
	if waitFor(ADD_AVATAR, ADD_AVATAR_COLOR) == -1: # Wait for avatar dialog
		return -1
	pyautogui.click()
	time.sleep(5)


	
	
	print("Visiting Textnow Site")
	pyautogui.moveTo(SEARCH_COORDS)
	pyautogui.click()
	pyautogui.typewrite('textnow.com')
	pyautogui.press('enter')
	time.sleep(7)
	pyautogui.moveTo(SEARCH_COORDS)
	pyautogui.click()
	time.sleep(2)
	pyautogui.click()
	pyautogui.typewrite('/signup')
	pyautogui.press('enter')
	time.sleep(10)

	# pyautogui.moveTo(REFRESH_COORDS)
	# pyautogui.click()
	# time.sleep(5)
	# Check if the page is loaded...
	load_textnow1 = True
	load_textnow2 = True

	pyautogui.moveTo(TEXTNOW_ICON1)
	if waitFor(TEXTNOW_ICON1, TEXTNOW_ICON_COLOR1) == -1: # Waiting for site to load
		load_textnow1 = False
		
	time.sleep(1)

	pyautogui.moveTo(TEXTNOW_ICON2)
	if waitFor(TEXTNOW_ICON2, TEXTNOW_ICON_COLOR2) == -1: # Waiting for site to load
		load_textnow2 = False

	print (load_textnow1)
	print (load_textnow2)

	if not((load_textnow1) or (load_textnow2)):
		return -1

	print("Captcha existing")
	pyautogui.moveTo(CAPTCHA_COORDS)

	global noCaptcha

	if waitFor(CHECK_SIGNUP, '#8D63E7') == -1: # Waiting for site to load
		print("An error occured.")
	else:
		print ("Already completed captcha.")
		noCaptcha = True
		return 2


	pyautogui.click()
	time.sleep(3)

	print("Downloading Captcha")
	
	pyautogui.moveTo(CHECK_COORDS)
	if waitFor(CHECK_COORDS, CHECK_COLOR) == -1: # Waiting for site to load
		print("An error occured.")
	else:
		print ("Already completed captcha.")
		return 2

	
		
	# if CHECK_COLOR in runCommand("eval $(xdotool getmouselocation --shell); xwd -root -silent | convert xwd:- -depth 8 -crop '1x1+$X+$Y' txt:- | grep -om1 '#\w\+'").decode("utf-8"):
	# 	print ("Already completed captcha.")
	# 	return 2

	pyautogui.moveTo(AUDIO_COORDS)
	pyautogui.click()
	time.sleep(2.5)
	
	pyautogui.moveTo(DOWNLOAD_COORDS)
	pyautogui.click()
	time.sleep(11)
	pyautogui.moveTo(AUDIO_PLAY_BAR)
	if waitFor(AUDIO_PLAY_BAR, PLAY_BAR_COLOR) == -1: # Waiting for site to load
		return -1
	pyautogui.rightClick()
	time.sleep(.3)
	pyautogui.moveTo(DOWNLOAD_AUDIO_ITME)
	pyautogui.click()
	time.sleep(1)
	pyautogui.moveTo(SAVE_BUTTON)
	pyautogui.click()
	time.sleep(2)
	pyautogui.moveTo(CLOSE_TAB)
	pyautogui.click()

	return 0


def checkCaptcha():
	''' Check if we've completed the captcha successfully. '''
	print (CHECK_COORDS)
	print ("check verify result")
	pyautogui.moveTo(CHECK_COORDS)
	if waitFor(CHECK_COORDS, CHECK_COLOR) == -1: # Waiting for site to load
		print("An error occured.")
		output = 0
	else:
		print ("Successfully completed captcha.")
		output = 1
	# print (runCommand("eval $(xdotool getmouselocation --shell); xwd -root -silent | convert xwd:- -depth 8 -crop '1x1+$X+$Y' txt:- | grep -om1 '#\w\+'"))
	# if CHECK_COLOR in runCommand("eval $(xdotool getmouselocation --shell); xwd -root -silent | convert xwd:- -depth 8 -crop '1x1+$X+$Y' txt:- | grep -om1 '#\w\+'").decode("utf-8"):
	# 	print ("Successfully completed captcha.")
	# 	output = 1
	# else:
	# 	print("An error occured.")
	# 	output = 0

	# pyautogui.moveTo(CLOSE_LOCATION)
	# pyautogui.click()
	return output

def secondCheckCaptcha():
	''' Check if we've completed the captcha successfully. '''
	print ("check verify result")
	pyautogui.moveTo(CONFIRM_NUMBER_BUTTON)
	if waitFor(CONFIRM_NUMBER_BUTTON, CONFIRM_NUMBER_COLOR) == -1: # Waiting for site to load
		print("An error occured.")
		output = 0
	else:
		print ("Successfully completed captcha.")
		output = 1

	pyautogui.click()
	time.sleep(1)
	pyautogui.click()
	# print (runCommand("eval $(xdotool getmouselocation --shell); xwd -root -silent | convert xwd:- -depth 8 -crop '1x1+$X+$Y' txt:- | grep -om1 '#\w\+'"))
	# if CHECK_COLOR in runCommand("eval $(xdotool getmouselocation --shell); xwd -root -silent | convert xwd:- -depth 8 -crop '1x1+$X+$Y' txt:- | grep -om1 '#\w\+'").decode("utf-8"):
	# 	print ("Successfully completed captcha.")
	# 	output = 1
	# else:
	# 	print("An error occured.")
	# 	output = 0

	# pyautogui.moveTo(CLOSE_LOCATION)
	# pyautogui.click()
	return output


def runCap(tempEmail):
	try:
		print("Removing old files...")
		os.system('rm ./audio.wav 2>/dev/null') # These files may be left over from previous runs, and should be removed just in case.
		os.system('rm ' + DOWNLOAD_LOCATION + 'audio.mp3 2>/dev/null')
		# First, download the file
		downloadResult = downloadCaptcha(tempEmail)
		if downloadResult == 2:
			# pyautogui.moveTo(CLOSE_LOCATION)
			# pyautogui.click()
			return 2
		elif downloadResult == -1:
			closeWinodws()
			return 3
		
		# Convert the file to a format our APIs will understand
		print("Converting Captcha...")
		os.system("echo 'y' | ffmpeg -i " + DOWNLOAD_LOCATION + "audio.mp3 ./audio.wav 2>/dev/null")
		with sr.AudioFile('./audio.wav') as source:
			audio = r.record(source)
		
		print("Submitting To Speech to Text:")
		determined = google(audio) # Instead of google, you can use ibm or bing here
		print(determined)

		print("Inputting Answer")
		# Input the captcha 
		pyautogui.moveTo(FINAL_COORDS)
		pyautogui.click()
		time.sleep(.5)
		pyautogui.typewrite(determined, interval=.05)
		time.sleep(.5)
		pyautogui.moveTo(VERIFY_COORDS)
		pyautogui.click()

		print("Verifying Answer")
		time.sleep(5)
		
		# Check that the captcha is completed
		
		result = checkCaptcha()
		return result
	except Exception as e:
		# pyautogui.moveTo(CLOSE_LOCATION)
		# pyautogui.click()
		closeWinodws()
		return 3

# Captcha
def downloadCaptchaAfterCreate():
	''' Navigate to demo site, input user info, and download a captcha. '''
	
	time.sleep(1)
	pyautogui.moveTo(AREA_CODE_INPUT)
	pyautogui.click()
	pyautogui.typewrite("645", interval=0.7)
	time.sleep(1)

	#Confirm Area Code Input
	pyautogui.moveTo(AREA_CODE_BUTTON)
	if waitFor(AREA_CODE_BUTTON, '#652CDE') == -1: # Waiting for site to load
		return -1
	pyautogui.click()
	time.sleep(4)

	#Solving Captcha
	pyautogui.moveTo(CAPTCHA_CHECK_BOX)
	pyautogui.click()
	time.sleep(4)
	

	print("Downloading Captcha")
	
	pyautogui.moveTo(CAPTCHA_CHECK_BOX)
	if waitFor(CAPTCHA_CHECK_BOX, CHECK_COLOR) == -1: # Waiting for site to load
		print("An error occured.")
	else:
		print ("Already completed captcha.")
		return 2

	time.sleep(5)

	pyautogui.moveTo(AFTER_AUDIO_COORDS)
	pyautogui.click()
	time.sleep(2.5)
	
	pyautogui.moveTo(AFTER_DOWNLOAD_COORDS)
	pyautogui.click()
	time.sleep(11)
	pyautogui.moveTo(AFTER_AUDIO_PLAY_BAR)
	if waitFor(AFTER_AUDIO_PLAY_BAR, PLAY_BAR_COLOR) == -1: # Waiting for site to load
		return -1
	pyautogui.rightClick()
	time.sleep(1)
	pyautogui.moveTo(AFTER_DOWNLOAD_AUDIO_ITME)
	pyautogui.click()
	time.sleep(2)
	pyautogui.moveTo(AFTER_SAVE_BUTTON)
	pyautogui.click()
	time.sleep(4)
	pyautogui.moveTo(DL_CLOSE_BUTTON)
	pyautogui.click()
	time.sleep(2)
	pyautogui.moveTo(CLOSE_TAB)
	pyautogui.click()


	return 0

def runCapAfterCreate():
	try:
		print("Removing old files...")
		os.system('rm ./audio.wav 2>/dev/null') # These files may be left over from previous runs, and should be removed just in case.
		os.system('rm ' + DOWNLOAD_LOCATION + 'audio.mp3 2>/dev/null')
		# First, download the file
		downloadResult = downloadCaptchaAfterCreate()
		if downloadResult == 2:
			# pyautogui.moveTo(CLOSE_LOCATION)
			# pyautogui.click()
			return 2
		elif downloadResult == -1:
			# pyautogui.moveTo(CLOSE_LOCATION)
			# pyautogui.click()
			closeWinodws()
			return 3
		
		# Convert the file to a format our APIs will understand
		print("Converting Captcha...")
		os.system("echo 'y' | ffmpeg -i " + DOWNLOAD_LOCATION + "audio.mp3 ./audio.wav 2>/dev/null")
		with sr.AudioFile('./audio.wav') as source:
			audio = r.record(source)
		
		print("Submitting To Speech to Text:")
		determined = google(audio) # Instead of google, you can use ibm or bing here
		print(determined)

		print("Inputting Answer of Textnow")
		# Input the captcha 
		pyautogui.moveTo(AFTER_FINAL_COORDS)
		pyautogui.click()
		time.sleep(.5)
		pyautogui.typewrite(determined, interval=.05)
		time.sleep(.5)
		pyautogui.moveTo(AFTER_VERIFY_COORDS)
		pyautogui.click()

		print("Textnow Verifying Answer")
		time.sleep(5)
		
		# Check that the captcha is completed
		
		result = secondCheckCaptcha()
		return result



	except Exception as e:
		# pyautogui.moveTo(CLOSE_LOCATION)
		# pyautogui.click()
		closeWinodws()
		return 3


def runCaptcha(tempEmail):
	success = 0
	fail = 0
	allowed = 0

	# Run this forever and print statistics
	while True:
		res = runCap(tempEmail)
		if res == 1:
			success += 1
		elif res == 2: # Sometimes google just lets us in
			allowed += 1
		else:
			fail += 1

		print("SUCCESSES: " + str(success) + " FAILURES: " + str(fail) + " Allowed: " + str(allowed))

		if (res==2) or (res==1):
			break

		closeWinodws()
		# pyautogui.moveTo(CLOSE_LOCATION)
		# pyautogui.click()

	#input email
	pyautogui.moveTo(EMAIL_INPUT)
	pyautogui.click()
	time.sleep(.5)
	pyautogui.typewrite(tempEmail, interval=.05)
	time.sleep(.5)
	#input password
	pyautogui.moveTo(PASSWORD_INPUT)
	pyautogui.click()
	time.sleep(.5)
	password = tempEmail.split("@")[0];
	print("password=", password)
	pyautogui.typewrite(password, interval=.4)
	time.sleep(1)
	pyautogui.typewrite("775588", interval=.3)

	#creatin new textnow account
	global noCaptcha
	print (noCaptcha)
	if noCaptcha :
		pyautogui.moveTo(NOCAPTCHA_SIGNUP_BUTTON)
		pyautogui.click()
	else:
		pyautogui.moveTo(SIGNUP_BUTTON)
		pyautogui.click()


	time.sleep(20) #waiting creating the textnow account.

	print ("Confirm the account creating.")	
	pyautogui.moveTo(CONFIRM_CREATE)
	if waitFor(CONFIRM_CREATE, CONFIRM_CREATE_COLOR) == -1: # Waiting for site to load
		closeWinodws()
		return -1
		# pyautogui.moveTo(CLOSE_LOCATION)
		# pyautogui.click()

	# After creating textnow account and passing captcha
	print("From Here it is the Captcha of Textnow account.")
	while True:
		res = runCapAfterCreate()
		if res == 1:
			success += 1
		elif res == 2: # Sometimes google just lets us in
			allowed += 1
		else:
			fail += 1

		print("SUCCESSES: " + str(success) + " FAILURES: " + str(fail) + " Allowed: " + str(allowed))

		if (res==2) or (res==1):
			break

		closeWinodws()
		# pyautogui.moveTo(CLOSE_LOCATION)
		# pyautogui.click()

	time.sleep(7)


	return 3
	



		
	