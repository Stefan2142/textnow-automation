# textnow-automation

Project in Python, done for a client. 

# About

Automated process of obtaining a temporary phone number form textnow.com, creating a temporary email in order to create account on Telegram. It includes working with Selenium and breaking Google's captcha. Developed on Linux VM on USA server.

